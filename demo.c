#include <x86intrin.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

// cat /proc/cpuinfo | grep MHz
#define CPU_MHZ 2494.324
#define CPU_HZ ((int64_t)(CPU_MHZ * 1000000))

double cycles_to_seconds(int64_t start, int64_t end)
{
    const int64_t total_cycles = end - start;
    return ((double)total_cycles) / CPU_HZ;
}

double timespec_to_seconds(struct timespec start, struct timespec end)
{
    return end.tv_sec - start.tv_sec + 0.000000001 * (end.tv_nsec - start.tv_nsec);
}

int main(void)
{
    struct timespec start_thread, end_thread, start_system, end_system;

    clock_gettime(CLOCK_REALTIME, &start_system);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID , &start_thread);
    const int64_t start_cycles = _rdtsc();

    double acc = 0.0;
    for (int64_t i = 0; i < 200000000ll; i += 1) {
        acc += sin((double) i);
    }
    printf("%lf\r", acc);

    clock_gettime(CLOCK_REALTIME, &end_system);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID , &end_thread);
    const int64_t end_cycles = _rdtsc();

    printf("[rdtsc] \t\t\t%lfs\n", cycles_to_seconds(start_cycles, end_cycles));
    printf("[CLOCK_REALTIME] \t\t%lfs\n", timespec_to_seconds(start_system, end_system));
    printf("[CLOCK_PROCESS_CPUTIME_ID] \t%lfs\n", timespec_to_seconds(start_thread, end_thread));

    return EXIT_SUCCESS;
}